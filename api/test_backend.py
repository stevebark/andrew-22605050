import boto3
import json
import os
import time

def test_backend(sqs):
    request_json = json.dumps({
        'job': 'foo', 
        'id': 123
        })

    print("Sending message to queue")
    job_queue.send_message(MessageBody=request_json)

    print("Waiting for a response")
    time.sleep(15)

    print("Printing all responses")
    for message in result_queue.receive_messages():
        print(message.body)
        message.delete()
    
    print("Done")

if __name__ == "__main__":
    started = time.time()
    while True:
        try:
            sqs = boto3.resource('sqs', endpoint_url=os.getenv('SQS_ENDPOINT_URL'), region_name=os.getenv('SQS_REGION_NAME'))
            break
        except Exception:
            #Waiting for sqs to become available
            if time.time() - started > 30:
                raise 
            else:
                print("Waiting for SQS to become available")
                time.sleep(1)
    job_queue = sqs.create_queue(QueueName='jobs')
    result_queue = sqs.create_queue(QueueName='results')        
    test_backend(sqs)
