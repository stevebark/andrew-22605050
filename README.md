# Senior Backend Developer - Technical Challenge
## Introduction

In this task, we’ll be assessing how you go about writing an API from scratch. 
The primary things we are looking out for and will judge on are: 

 * Your approach to the task
 * Adherence to good coding principles e.g clean, testable code
 * What you can get working in a short space of time. 
 
This challenge uses Python 3, Docker and docker-compose, so these should be installed in your development environment before you begin.  

## The Task

Write a RESTful API that provides an interface to two subtly-different types of long-running processes or “jobs”. These job types are named “foo” and “bar”.

Using the API you write for this challenge, you should be able to demonstrate the process for:

 * Creating a new “job” of either “foo” or “bar” job type.
 * Reading the status of a job (is it running / complete / in an error state)?
 * Reading results from completed jobs - the contents of which will vary by job type

It is not necessary to create a user interface or harness for your API, beyond anything needed to test it and demonstrate that it works.

## Resources

### Repository

Please clone this repository.

### Stack

A docker-compose file has been preconfigured with [localstack](https://github.com/localstack/localstack) (serving AWS services for SQS), MySQL for state storage, and the job-processing service that will be doing “the work” that your API will be responsible for interfacing to.

Clone the repository and run the following command to build the job-processing service and get started:

    docker-compose up -d

If all goes well, you should be able to access the basic localstack user interface here: http://localhost:8080/

### Implementation

You should add your API service code in the `./api` subdirectory, and amend the Dockerfile in that directory according to your own build requirements. You can delete all of the files in that directory if you wish - `./api/test_backend.py` has been provided as a convenience so that you can see working code for interacting with SQS using localstack.

When you’re ready to build your API, you should uncomment the “api” section of the `docker-compose.yml` file, providing any additional configuration you need. When you have finished this work, we will be able to integration-test the whole stack by using docker compose.

We recommend either Flask or Django Rest Framework to build your API - but you are free to use whatever you like.

#### Create a job

A client of the API needs to specify two things when starting a new job: a job type (either “foo” or “bar”) and a unique ID (integer) to identify that job in future.

When creating new jobs, the client should provide an ID field that will uniquely identify that job. This field must be an integer - other values should be rejected.

#### Invoke a new job

There is a ‘backend’ microservice in our docker-compose stack which will simulate performing long-running tasks. You should interact with this process by using an SQS queue (Amazon’s Simple Queue Service). This queue service is simulated using localstack - so you should find it available on http://localhost:4576 (or http://localstack:4576 within your container) once `docker-compose up -d` has been called.

Please refer to the `backend/jobprocessor/processor.py` code if you need to see an example of how to interact with SQS in this local environment.

To start a new job post a message to SQS queue called “jobs” using a json message in the following format:

    {
        'job': 'foo',
        'id': 123
    }

(NB: This queue is created when the backend process first runs)

Results can be read from an SQS queue called “results”, and if successful they will be json-encoded in the following format:

    {
        'request': {
            'job': 'foo',
            'id': 123
        },
        'result': {
            'foo': 'bar'
        }
    }

If the job fails, a message will be created in the following format:

    {
        'request': {
            'job': 'foo',
            'id': 123
        },
        'error': {
            'message': 'The job failed'
        }
    }

Each job takes 10 seconds to complete. But as the backend is single-threaded, jobs will be processed in serial - so you might have to wait longer if you submit a large number of requests!

#### Whilst Jobs are Running

Your API should allow clients to query the status of any jobs they have created.

#### Retrieve and Store Results

It is up to you how and when you retrieve results - but bear in mind that we might need to run multiple copies of the API process in production, so your design should take this into consideration.

You can use the MySQL database created by docker-compose to store state, or another stateful database store of your choice. It’s important that if the API process is re-created, it’s still possible to retrieve historical results.

Your API should allow clients to see all the data returned by the backend process. 

## Things to Think About

You will probably not have time to get a production-ready service completed in the time you have available, however, thinking about the following topics and being able to talk us through your thought process is expected:

1. **Error and failure handling** - how should failed jobs be handled?
2. **Scalability** - how would you scale the API for load in production?
3. **Testing** - how would you test your API and ensure bugs aren’t silently committed to the codebase.
4. **Performance** - how would you make the API flow more efficient in production? Can you think of a way to avoid clients needing to poll the API for status?
5. **Logging** - what would you use to ship logs out of the container?
6. **Documentation** - what facilities should you provide to clients learning the API?
7. **Extensibility** - what happens if a third or fourth job type is added? 
8. **Security** - how would you protect this API from unauthorised access, if the requirements demanded it in future?
9. **Deployment** - how are database schema updates going to be handled?
